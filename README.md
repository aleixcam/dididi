# DiDiDi

Tiny Inversion of Control Container for JavaScript

* Can inject services and add paramaters just with the use of simple strings and the power of RegExp.
* Its ease to use makes it very handy to manage dependencies from the tiniest of the apps, to the biggest.
* Tinier than your ex's heart.
* Inspired by Symfony's Dependency Injection.

## Usage

### Installation

```
npm install dididi
```

### Getting Started

The fist step to everything is getting an instance of the container. It can be easily achieved by:

```javascript
const container = require('dididi')()
```

Now that we have the container, let's see what it can do for us.

### Setting Paramaters

To set parameters for use in the container you have to pass and object with any given key/value pairs:

```javascript
container.parameters({
	parameter: 'value'
})
```

Parameters also accept injecting other parameters:

```javascript
container.parameters({
	parameter_in_parameter: 'onset_<parameter>' // onset_value
})
```

Or environment variables:

```javascript
container.parameters({
	environment_in_parameter: '<<NODE_ENV>>' // 'production' or 'development'
})
```

You can also add and overwrite parameters at any point in your project.

### Register a Service

To register a service you need an string identifier that needs to be in camel case and separated by dots and its constructor like so:

```javascript
class MockService {}

container.register(
	'example.mock_service',
	MockService
)
```

You can also add arguments:

```javascript
class MockServiceWithArgs {
	constructor(title, description, random) {
		this.title = title // 'an argument'
		this.description = description // 'can really be anything'
		this.random = random // whatever random number
	}
}

container.register(
	'example.mock_service_with_args',
	MockServiceWithArgs,
	[
		'an argument',
		'can really be anything',
		Math.random()
	]
)
```

To inject parameters to a service, using the setted parameter from before:

```javascript
class MockServiceWithParams {
	constructor(parameter) {
		this.parameter = parameter // 'value'
	}
}

container.register(
	'example.mock_service_with_params',
	MockServiceWithParams,
	[
		'<parameter>'
	]
)
```

You can also use the parameters inside your process.env global:


```javascript
class MockServiceWithEnv {
	constructor(environment) {
		this.environment = environment // 'production' or 'development'
	}
}

container.register(
	'example.mock_service_with_env',
	MockServiceWithEnv,
	[
		'<<NODE_ENV>>'
	]
)
```

And last but not least, you can inject dependencies:

```javascript
class MockServiceWithDependency {
	constructor(mockService) {
		this.mockService = mockService // MockService
	}
}

container.register(
	'example.mock_service_with_dependency',
	MockServiceWithDependency,
	[
		'>>example.mock_service'
	]
)
```

### Getting a Service

To get a Service you just need to use its identifier like that:

```javascript
/** @var {MockServiceWithDependency} mockServiceWithDependency */
const mockServiceWithDependency = container.get('example.mock_service_with_dependency')
```

## License

[MIT](https://gitlab.com/aleixcam/dididi/-/blob/master/LICENSE)
