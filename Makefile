all: help

## tests: Run tests suites
tdd:
	docker-compose run --rm node npm run test:watch

## shell: Interactive shell to use commands inside the container
shell:
	docker-compose run --rm node sh

## down: Stop and remove the container
down:
	docker-compose down

## pull: Update docker image
pull:
	docker-compose pull

## help: Show this screen
help: Makefile
	@sed -n 's/^##//p' $<
