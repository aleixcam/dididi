const { assert } = require('chai')
const Container = require('../../src/Application/Container')
const MockService = require('../Infrastructure/Service/MockService')
const DuplicateServiceError = require('../../src/Domain/Error/DuplicateServiceError')
const InvalidArgumentError = require('../../src/Application/Error/InvalidArgumentError')

describe('ContainerTest', () => {
	const A_STRING = 'argument'
	const A_NUMBER = 1
	const A_BOOLEAN = true
	const AN_OBJECT = { key: 'value' }
	const A_ENVIRONMENT_KEY = 'ENV_KEY'
	const A_ENVIRONMENT_VALUE = 'env_value'
	const A_PARAMETER_KEY = 'test_key'
	const A_PARAMETER_VALUE = 'test_value'
	const SOME_PARAMS = { [A_PARAMETER_KEY]: A_PARAMETER_VALUE, test_logger: 'testLogger' }
	const SOME_MORE_PARAMS = { test_logger: 'nullLogger', test_database: 'testDatabase' }
	const A_SERVICE_ID = 'infrastructure.service.mock_service'
	const A_SERVICE_WITH_PARAMS = 'infrastructure.service.mock_service_with_parameters'
	const A_SERVICE_WITH_DEPENDENCY = 'infrastructure.service.mock_service_with_dependency'
	const A_SERVICE_CONSTRUCTOR = MockService

	before(() => {
		process.env[A_ENVIRONMENT_KEY] = A_ENVIRONMENT_VALUE

		this.container = new Container()
	})

	it('should be able to add parameters', () => {
		this.container.parameters(SOME_PARAMS)

		assert.deepEqual(this.container._parameters, SOME_PARAMS)
	})

	describe('should be able to add parameters', () => {
		const cases = {
			'string': A_STRING,
			'number': A_NUMBER,
			'boolean': A_BOOLEAN,
			'object': AN_OBJECT
		}

		for (let test in cases) {
			it(`of ${test} type`, () => {
				this.container.parameters({ [test]: cases[test] })

				assert.deepEqual(this.container.getParameter(test), cases[test])
			})
		}
	})

	it('should be able to add and rewrite more parameters', () => {
		this.container.parameters(SOME_MORE_PARAMS)

		assert.deepEqual(this.container._parameters, { ...SOME_PARAMS, ...SOME_MORE_PARAMS })
	})

	it('should be able to get a parameter', () => {
		const parameter = this.container.getParameter(A_PARAMETER_KEY)

		assert.deepEqual(parameter, A_PARAMETER_VALUE)
	})

	describe('should be able to add parameters into a parameter', () => {
		const PARAMETER_KEY = 'parameter'

		const cases = {
			'one parameter': {
				test: { [PARAMETER_KEY]: `<${A_PARAMETER_KEY}>` },
				expect: A_PARAMETER_VALUE
			},
			'multiple parameters': {
				test: { [PARAMETER_KEY]: `<${A_PARAMETER_KEY}>-<${A_PARAMETER_KEY}>` },
				expect: `${A_PARAMETER_VALUE}-${A_PARAMETER_VALUE}`
			},
			'one environment': {
				test: { [PARAMETER_KEY]: `<<${A_ENVIRONMENT_KEY}>>` },
				expect: A_ENVIRONMENT_VALUE
			},
			'multiple environments': {
				test: { [PARAMETER_KEY]: `<<${A_ENVIRONMENT_KEY}>>-<<${A_ENVIRONMENT_KEY}>>` },
				expect: `${A_ENVIRONMENT_VALUE}-${A_ENVIRONMENT_VALUE}`
			},
			'parameter and environment': {
				test: { [PARAMETER_KEY]: `<${A_PARAMETER_KEY}>-<<${A_ENVIRONMENT_KEY}>>` },
				expect: `${A_PARAMETER_VALUE}-${A_ENVIRONMENT_VALUE}`
			}
		}

		for (let test in cases) {
			it(`with ${test}`, () => {
				this.container.parameters(cases[test].test)

				assert.deepEqual(this.container.getParameter(PARAMETER_KEY), cases[test].expect)
			})
		}
	})

	describe('should not be able to add parameters into a parameter', () => {
		const cases = {
			'empty environment': { parameter: '<<>>' },
			'invalid environment': { parameter: '<<invention>>' },
			'empty parameter': { parameter: '<>' },
			'invalid parameter': { parameter: '<invention>' },
			'service injection': { parameter: `>>${A_SERVICE_ID}` }
		}

		for (let test in cases) {
			it(`with ${test}`, () => {
				assert.throws(() => this.container.parameters(cases[test]), InvalidArgumentError)
			})
		}
	})

	it('should be able to register a new service', () => {
		this.container.register(A_SERVICE_ID, A_SERVICE_CONSTRUCTOR)

		const service = this.container._services.get(A_SERVICE_ID)

		assert.deepEqual(service.constructor, A_SERVICE_CONSTRUCTOR)
	})

	it('should not be able to register a service with repeated id', () => {
		assert.throws(() => this.container.register(
			A_SERVICE_ID,
			A_SERVICE_CONSTRUCTOR
		), DuplicateServiceError)
	})

	it('should be able to get a service', () => {
		const service = this.container.get(A_SERVICE_ID)

		assert.instanceOf(service, A_SERVICE_CONSTRUCTOR)
		assert.isEmpty(service.args)
	})

	it('should be able to register a new service with arguments', () => {
		this.container.register(
			A_SERVICE_WITH_PARAMS,
			A_SERVICE_CONSTRUCTOR,
			[
				A_STRING,
				A_NUMBER,
				A_BOOLEAN,
				AN_OBJECT,
				`<${A_PARAMETER_KEY}>`,
				`<${A_PARAMETER_KEY}>-<${A_PARAMETER_KEY}>`,
				`<<${A_ENVIRONMENT_KEY}>>`,
				`<<${A_ENVIRONMENT_KEY}>>-<<${A_ENVIRONMENT_KEY}>>`,
				`<${A_PARAMETER_KEY}>-<<${A_ENVIRONMENT_KEY}>>`,
			]
		)

		const service = this.container.get(A_SERVICE_WITH_PARAMS)

		assert.instanceOf(service, A_SERVICE_CONSTRUCTOR)
		assert.include(service.args, A_STRING)
		assert.include(service.args, A_NUMBER)
		assert.include(service.args, A_BOOLEAN)
		assert.deepInclude(service.args, AN_OBJECT)
		assert.include(service.args, A_PARAMETER_VALUE)
		assert.include(service.args, `${A_PARAMETER_VALUE}-${A_PARAMETER_VALUE}`)
		assert.include(service.args, A_ENVIRONMENT_VALUE)
		assert.include(service.args, `${A_ENVIRONMENT_VALUE}-${A_ENVIRONMENT_VALUE}`)
		assert.include(service.args, `${A_PARAMETER_VALUE}-${A_ENVIRONMENT_VALUE}`)
	})

	it('should be able to register a new service with a dependency', () => {
		this.container.register(
			A_SERVICE_WITH_DEPENDENCY,
			A_SERVICE_CONSTRUCTOR,
			[
				`>>${A_SERVICE_ID}`,
				`>>${A_SERVICE_WITH_PARAMS}`
			]
		)

		const service = this.container.get(A_SERVICE_WITH_DEPENDENCY)
		const injectedService = service.args[0]
		const injectedServiceWithParams = service.args[1]

		assert.instanceOf(service, A_SERVICE_CONSTRUCTOR)
		assert.instanceOf(injectedService, A_SERVICE_CONSTRUCTOR)
		assert.isEmpty(injectedService.args)
		assert.instanceOf(injectedServiceWithParams, A_SERVICE_CONSTRUCTOR)
		assert.include(injectedServiceWithParams.args, A_PARAMETER_VALUE)
		assert.include(injectedServiceWithParams.args, `${A_PARAMETER_VALUE}-${A_PARAMETER_VALUE}`)
		assert.include(injectedServiceWithParams.args, A_ENVIRONMENT_VALUE)
		assert.include(injectedServiceWithParams.args, `${A_ENVIRONMENT_VALUE}-${A_ENVIRONMENT_VALUE}`)
		assert.include(injectedServiceWithParams.args, `${A_PARAMETER_VALUE}-${A_ENVIRONMENT_VALUE}`)
	})

	describe('should not be able to register a new service', () => {
		const cases = {
			'empty environment': '<<>>',
			'invalid environment': '<<invention>>',
			'empty parameter': '<>',
			'invalid parameter': '<invention>',
			'empty dependency': '>>',
			'invalid dependency': '>>invention'
		}

		for (let test in cases) {
			it(`with ${test}`, () => {
				const SERVICE_ID = test.replace(/ /g, '_')

				this.container.register(
					SERVICE_ID,
					A_SERVICE_CONSTRUCTOR,
					[
						cases[test]
					]
				)

				assert.throws(() => this.container.get(SERVICE_ID), InvalidArgumentError)
			})
		}
	})
})
