class MockService {
	constructor() {
		this.args = []
		for (let i in arguments) {
			this.args[i] = arguments[i]
		}
	}
}

module.exports = MockService
