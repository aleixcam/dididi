const { assert } = require('chai')
const Service = require('../../src/Domain/Service')
const InvalidServiceError = require('../../src/Domain/Error/InvalidServiceError')

describe('ServiceTest', () => {
	const AN_ID = 'test'
	const A_CONSTRUCTOR = Function
	const SOME_ARGUMENTS = [
		'testValue',
		'>>test.service',
		'<test.parameter>'
	]

	it('should be able to create', () => {
		const service = new Service({
			id: AN_ID,
			constructor: A_CONSTRUCTOR
		})

		assert.strictEqual(service.id, AN_ID)
		assert.strictEqual(service.constructor, A_CONSTRUCTOR)
		assert.deepEqual(service.args, [])
		assert.isTrue(Object.isFrozen(service))
	})

	it('should be able to create with arguments', () => {
		const service = new Service({
			id: AN_ID,
			constructor: A_CONSTRUCTOR,
			args: SOME_ARGUMENTS
		})

		assert.strictEqual(service.id, AN_ID)
		assert.strictEqual(service.constructor, A_CONSTRUCTOR)
		assert.deepEqual(service.args, SOME_ARGUMENTS)
		assert.isTrue(Object.isSealed(service))
	})

	describe('should not be able to create when id is invalid', () => {
		const cases = {
			'invalid snake case': 'test.matcher._function',
			'uppercase': 'test.matcherFunction',
			'numbers': 'test.matcher_function_1',
			'invalid separator': 'test.matcher_function.'
		}

		for (let test in cases) {
			it(`from ${test}`, () => {
				assert.throws(() => new Service({
					id: cases[test],
					constructor: A_CONSTRUCTOR
				}), InvalidServiceError, `Cannot declare a Service with the id '${cases[test]}'`)
			})
		}
	})

	it('should not be able to create when constructor is not a function', () => {
		assert.throws(() => new Service({
			id: AN_ID,
			constructor: ''
		}), InvalidServiceError, 'The Service constructor is not a function')
	})
})
