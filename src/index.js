const Container = require('./Application/Container')

module.exports = () => new Container()
