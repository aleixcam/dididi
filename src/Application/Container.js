const Service = require('../Domain/Service')
const DuplicateServiceError = require('../Domain/Error/DuplicateServiceError')
const InvalidArgumentError = require('./Error/InvalidArgumentError')

const ENVIRONMENT_MATCHER = /<<.*?>>/g
const PARAMETER_MATCHER = /<.*?>/g
const DEPENDENCY_MATCHER = /(?<=^>>).*/g

class Container {
	constructor() {
		this._parameters = {}
		this._services = new Map()
	}

	parameters(parameters) {
		for (let parameter in parameters) {
			this._guardFromInvalidParameter(parameters[parameter])

			parameters[parameter] = this._replaceEnvironment(parameters[parameter])
			parameters[parameter] = this._replaceParameters(parameters[parameter])

			this._parameters = { ...this._parameters, [parameter]: parameters[parameter] }
		}
	}

	getParameter(key) {
		return this._parameters[key]
	}

	register(id, constructor, args) {
		this._guardFromDuplicateServiceId(id)

		const service = new Service({ id, constructor, args })

		this._services.set(service.id, {
			constructor: service.constructor,
			args: service.args
		})
	}

	get(id) {
		const { constructor, args } = this._services.get(id)

		const parsedArguments = args
			.map(arg => JSON.stringify(arg))
			.map(arg => this._replaceEnvironment(arg))
			.map(arg => this._replaceParameters(arg))
			.map(arg => this._injectDependencies(arg))

		return new constructor(...parsedArguments)
	}

	_replaceEnvironment(arg) {
		if (typeof arg !== 'string') return arg

		const matches = arg.match(ENVIRONMENT_MATCHER)

		if (!matches) return arg

		matches.forEach(key => {
			const parameter = process.env[key.replace(/<|>/g, '')]

			this._guardFromInvalidParameter(parameter)

			arg = arg.replace(new RegExp(key), parameter)
		})

		return arg
	}

	_replaceParameters(arg) {
		if (typeof arg !== 'string') return arg

		const matches = arg.match(PARAMETER_MATCHER)

		if (!matches) return arg

		matches.forEach(key => {
			const parameter = this._parameters[key.replace(/<|>/g, '')]

			this._guardFromInvalidParameter(parameter)

			arg = arg.replace(new RegExp(key), parameter)
		})

		return arg
	}

	_injectDependencies(arg) {
		if (typeof arg !== 'string') return arg

		const parsed = JSON.parse(arg)

		if (typeof parsed !== 'string') return parsed

		const matches = parsed.match(DEPENDENCY_MATCHER)

		if (!matches) return parsed

		this._guardFromInvalidDependency(matches[0])

		return this.get(matches[0])
	}

	_guardFromDuplicateServiceId(id) {
		if (this._services.get(id)) {
			throw new DuplicateServiceError(id)
		}
	}

	_guardFromInvalidParameter(parameter) {
		if (!parameter || typeof parameter === 'string' && parameter.match(DEPENDENCY_MATCHER)) {
			throw new InvalidArgumentError()
		}
	}

	_guardFromInvalidDependency(dependency) {
		if (!dependency || !this._services.has(dependency)) {
			throw new InvalidArgumentError()
		}
	}
}

module.exports = Container
