class InvalidArgumentError extends Error {
	constructor() {
		super('Could not get a Service with an invalid argument')
		this.name = this.constructor.name
	}
}

module.exports = InvalidArgumentError
