const InvalidServiceError = require('./Error/InvalidServiceError')

const ID_MATCHER = /^[a-z]+(?:_[a-z]+)*(?:\.[a-z]+(?:_[a-z]+)*)*$/

class Service {
	constructor({ id, constructor, args }) {
		this._setId(id)
		this._setConstructor(constructor)
		this.args = args || []

		Object.freeze(this)
	}

	_setId(id) {
		if (!ID_MATCHER.test(id)) {
			throw InvalidServiceError.fromId(id)
		}

		this.id = id
	}

	_setConstructor(constructor) {
		if (!(constructor instanceof Function)) {
			throw InvalidServiceError.fromConstructor()
		}

		this.constructor = constructor
	}
}

module.exports = Service
