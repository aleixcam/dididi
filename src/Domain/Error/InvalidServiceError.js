class InvalidServiceError extends Error {
	constructor(message) {
		super(message)
		this.name = this.constructor.name
	}

	static fromId(id) {
		return new this(`Cannot declare a Service with the id '${id}'`)
	}

	static fromConstructor() {
		return new this('The Service constructor is not a function')
	}
}

module.exports = InvalidServiceError
