class DuplicateServiceError extends Error {
	constructor(id) {
		super(`Cannot register Service with duplicate id '${id}'`)
		this.name = this.constructor.name
	}
}

module.exports = DuplicateServiceError
